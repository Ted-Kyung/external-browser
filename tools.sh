#!/bin/sh

# MADE BY TED .. 
# HOW TO USE THIS SCRIPT??? 
# 
# ./tools.sh build {os}      >>> cordova build 
# ./tools.sh run {os} {simulator-version} 8, 9, X, XS ...  
# ./tools.sh reset          >>>> remove dependencies, re install dependencies 

cmd="$1"
os="$2"

#if cmd : scp
#         $0      $1  $2 
#use : ./tools.sh cmd ip
#
if [ $cmd = reset ]
then
  npm cache clean --force
  rm -rf plugins
  rm -rf node_modules
  rm -rf platforms
  rm package-lock.json
  npm install
  ionic cordova prepare ios
  ionic cordova prepare android
fi


if [ $cmd = build ]
then
  if [ $os = ios ]
  then
    ionic cordova build ios -- --buildFlag="-UseModernBuildSystem=0"
  fi
  if [ $os = android ]
  then
    ionic cordova build android
  fi
fi

if [ $cmd = run ]
then
  if [ $os = ios ]
  then
    ionic cordova run ios --livereload -- --buildFlag="-UseModernBuildSystem=0"
  fi
  if [ $os = android ]
  then
    ionic cordova run android --livereload
  fi
fi








