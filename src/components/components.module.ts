import { NgModule } from '@angular/core';
import { LoginPhoneNumberComponent } from './login-phone-number/login-phone-number';
import { IonicModule } from 'ionic-angular';
import { ContryCodeComponent } from './contry-code/contry-code';
import { VerificationModalComponent } from './verification-modal/verification-modal';


@NgModule({
	declarations: [LoginPhoneNumberComponent,
    ContryCodeComponent,
    VerificationModalComponent],
	imports: [IonicModule],
	exports: [LoginPhoneNumberComponent,
    ContryCodeComponent,
    VerificationModalComponent]
})
export class ComponentsModule {}
