import { Component, EventEmitter, Input, Output, ViewChild } from '@angular/core';

import { LangServices } from '../../services/languageService';

/**
 * Generated class for the VerificationModalComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'verification-modal',
  templateUrl: 'verification-modal.html'
})
export class VerificationModalComponent {
  @ViewChild('verifiednumber') verifiednumber;
  @Output() verifiedHandle=new EventEmitter();

  validVerifiedNumber = 0;

  constructor(private langSrv: LangServices) {
  }

  getVerifiedButtonStyle(){
    if(this.validVerifiedNumber === 1){
      return {'background-color':'#194bff'};
    } else {
      return {'background-color':'#a5a5a5'};
    }
  }
  onVerifiedChange(){
    console.log("on verified change : " + this.verifiednumber.value);
    if(this.verifiednumber.value.length == 6){
      this.validVerifiedNumber = 1;
    } else {
      this.validVerifiedNumber = 0;
    }
  }
  verified(){
    console.log("check fb verification :"  + this.verifiednumber.value);
    this.verifiedHandle.emit({code:this.verifiednumber.value});
  }
}
