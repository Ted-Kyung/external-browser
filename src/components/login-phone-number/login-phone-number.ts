import { AlertController, LoadingController } from 'ionic-angular';
import { Component, EventEmitter, Input, Output, ViewChild } from '@angular/core';

import {FirebaseLoginService} from '../../services/fbLoginService';

import {FirebaseRestApiServices} from '../../services/fbRestApi';
import { LangServices } from '../../services/languageService';

/**
 * Generated class for the LoginPhoneNumberComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'login-phone-number',
  templateUrl: 'login-phone-number.html'
})
export class LoginPhoneNumberComponent {
  @ViewChild('phonenumber') phone;

  // @Input() user;
  @Output() resultCb = new EventEmitter();

  openCodeFinder = false;
  openVerified = false;

  validNumber = 0;
  validVerifiedNumber = 0;

  dial_code = "";

  verificationId = null;

  constructor(public loginService:FirebaseLoginService,
              public loading:LoadingController,
              public restApi:FirebaseRestApiServices,
              public alertCtrl:AlertController,
              private langSrv: LangServices) {
  }


  validCheckStyle(){
    if((this.validNumber === 1) || (this.validNumber === 3)){
      document.getElementById('valid-res').innerText = this.langSrv.getLangData('correct phone number');
      return {'color':'#508251'};
    } else if(this.validNumber === 0) {
      document.getElementById('valid-res').innerText = this.langSrv.getLangData('please choice code number');
      return {'color':'#ff8888'};
    } else if(this.validNumber === 2) {
      document.getElementById('valid-res').innerText = this.langSrv.getLangData('incorrect phone number');
      return {'color':'#ff8888'};
    }
  }

  getVerificationButtonStyle(){
    if((this.validNumber === 1)){
      return {'background-color':'#194bff'};
    } else {
      return {'background-color':'#a5a5a5'};
    }
  }


  codeResultHandler(data){
    console.log("code result : " + data.res);
    this.openCodeFinder = false;

    // set
    document.getElementById('code').innerText = data.res.dial_code;
    this.dial_code = data.res.dial_code;
  }

  findCode(){
    console.log("findCode Click");
    this.openCodeFinder = true;
  }

  onChange(){
      console.log("on change : " + this.phone.value);
      if(this.dial_code.length>0 && this.phone.value){
        this.validationCheckPhoneNumber(this.dial_code + this.phone.value);
      } else {
        this.validNumber = 0;
      }
  }

  validationCheckPhoneNumber(number:string){
      console.log("check regex : " + number);
      let re = /^[+]*[0-9]{0,1}[-\s\./0-9]*$/g;
      let result = re.test(number);
      if(result) {
        this.validNumber = 1;
      }
      else {
        this.validNumber = 2;
      }
  }

  public async sendVerificationMMS(){
    let slackUrl = 'https://hooks.slack.com/services/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';
    if(this.validNumber !== 1) return ;
    const _l = await this.loading.create({ spinner: 'crescent', duration: 5000 });
    _l.present();
    this.openVerified = true;
    this.validNumber = 3;
    var _phone:string = this.phone.value;

    if(_phone[0] === '0') {
      _phone = _phone.slice(1);
    }
    let number = '' + this.dial_code + _phone;

    let res = await this.restApi.restApiWithData("getUserItem", {phoneNumber: number});
    if(!!res.result){
      _l.dismiss();
      let msg = {"text": "ReLogin user : " + number};
      this.restApi.restApiWithUrlAndData(slackUrl, msg);
      this.resultCb.emit({user:res.result, isNew:false});
    }
    else {
      console.log("click verification mms send: " + number);
      this.verificationId = await this.loginService.phoneNumberVerification(number);
      _l.dismiss();
    }
  }

  public async checkVerified(data){
    let slackUrl = 'https://hooks.slack.com/services/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';
    console.log("handle verification mms res : " + data.code);
    // todo : close this page
    const _l = await this.loading.create({ spinner: 'crescent', duration: 5000 });
    _l.present();
    try{
      let result = await this.loginService.phoneNumberVerifiedCodeConfirm(data.code);
      _l.dismiss();
      if(result){
        console.log("login success : " + JSON.stringify(result));
        let msg = {"text": "New User Created : " + result.phoneNumber};
        this.restApi.restApiWithUrlAndData(slackUrl, msg);
        this.resultCb.emit({user:result, isNew:true});
      } else {
        console.log("login failed");
      }
    }
    catch (e){
      _l.dismiss();
      console.log("error : " + e);
      let _alert = this.alertCtrl.create({
        title: this.langSrv.getLangData("ERROR"),
        subTitle: this.langSrv.getLangData("incorrect code")
      });
      _alert.present()
    }
  }
}
