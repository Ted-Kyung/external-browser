import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { HttpClientModule } from '@angular/common/http';
import { MyApp } from './app.component';

import { HomePage } from '../pages/home/home';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { HttpModule } from '@angular/http';

import { AdMobFree } from '@ionic-native/admob-free';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { LocalNotifications } from '@ionic-native/local-notifications';
import { Badge } from '@ionic-native/badge';

import {HTTP} from '@ionic-native/http';
import { DNS } from '@ionic-native/dns';

//services
import { FirebaseLoginService } from '../services/fbLoginService';
import { FirebaseDatabaseServices} from '../services/fbDatabaseService';
import { FirebaseRestApiServices } from '../services/fbRestApi';
import { LangServices } from '../services/languageService';
import { Language } from '../services/i18n/language';

// component
import { ComponentsModule } from '../components/components.module';
// local storage 
import { IonicStorageModule } from '@ionic/storage';
//keyboard
import { Keyboard } from '@ionic-native/keyboard/ngx';
//firebase app 
import { Firebase } from '@ionic-native/firebase/ngx';
import * as firebase from 'firebase/app';

import { AppVersion } from '@ionic-native/app-version';

// paypal
// import { PayPal } from '@ionic-native/paypal';
import { InAppPurchase } from '@ionic-native/in-app-purchase';

// viewport
import { MobileAccessibility } from '@ionic-native/mobile-accessibility';

import { AndroidPermissions } from '@ionic-native/android-permissions/ngx';

// Initialize Firebase
var config = {
  apiKey: "AIzaSyCkFdsYTQJ_U-vBcFPw_QCoC_-FM_XQk00",
  authDomain: "futurebot-2ce20.firebaseapp.com",
  databaseURL: "https://futurebot-2ce20.firebaseio.com",
  projectId: "futurebot-2ce20",
  storageBucket: "futurebot-2ce20.appspot.com",
  messagingSenderId: "702288092775"
};
firebase.initializeApp(config);

@NgModule({
  declarations: [
    MyApp,
    HomePage,
  ],
  imports: [
    BrowserModule,
    HttpModule,
    ComponentsModule,
    HttpClientModule,
    IonicStorageModule.forRoot(),
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
  ],
  providers: [
    StatusBar,
    SplashScreen,
    AdMobFree,
    LocalNotifications,
    FirebaseLoginService,
    FirebaseDatabaseServices,
    FirebaseRestApiServices,
    LangServices,
    Language,
    Badge,
    HTTP,
    DNS,
    Keyboard,
    Firebase,
    AppVersion,
    AndroidPermissions,
    InAppBrowser,
    InAppPurchase,
    MobileAccessibility
    // {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
