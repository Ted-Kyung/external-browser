import { Component } from '@angular/core';
import { Platform, Events } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { MobileAccessibility } from '@ionic-native/mobile-accessibility';

import { HomePage } from '../pages/home/home';

import { AdMobFree, AdMobFreeBannerConfig, AdMobFreeInterstitialConfig } from '@ionic-native/admob-free';
import { LocalNotifications } from '@ionic-native/local-notifications';
import { Badge } from '@ionic-native/badge';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage:any = HomePage;

  constructor(public platform: Platform,
              private statusBar: StatusBar,
              private splashScreen: SplashScreen,
              private events:Events,
              private mobileAccessibility: MobileAccessibility,
              private admob: AdMobFree,
              private lnoti:LocalNotifications,
              private badge: Badge) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
      this._setScreenViewPort();

      this.badge.clear();
    });
    platform.pause.subscribe(() => {
      console.log("NNNNNNNNNNNNN back");
      this.lnoti.schedule([{
        text: '18 Hour Left',
        trigger: {at: new Date(new Date().getTime() + (6 * 60 * 60 * 1000) )},
        led: 'FF0000',
        sound: 'default',
        badge: 6,
        vibrate: true,
        id: 0,
      },{
        text: '12 Hour Left',
        trigger: {at: new Date(new Date().getTime() + (12 * 60 * 60 * 1000) )},
        led: 'FF0000',
        sound: 'default',
        badge: 12,
        vibrate: true,
        id: 1,
      },{
        text: '6 Hour Left',
        trigger: {at: new Date(new Date().getTime() + (18 * 60 * 60 * 1000) )},
        led: 'FF0000',
        sound: 'default',
        badge: 18,
        vibrate: true,
        id: 2,
      },{
        text: 'WARRING!!!! 1 Hour Left',
        trigger: {at: new Date(new Date().getTime() + (23 * 60 * 60 * 1000) )},
        led: 'FF0000',
        sound: 'default',
        badge: 23,
        vibrate: true,
        id: 3,
      },{
        text: 'OVER 24 Hour!!',
        trigger: {at: new Date(new Date().getTime() + (24 * 60 * 60 * 1000) )},
        led: 'FF0000',
        sound: 'default',
        badge: 24,
        vibrate: true,
        id: 4,
      }]);
    });
    platform.resume.subscribe(() => {
      console.log("NNNNNNNNNNNNN on");
      this.events.publish("display");
      
      this.lnoti.clear(0);
      this.lnoti.clear(1);
      this.lnoti.clear(2);
      this.lnoti.clear(3);
      this.lnoti.clear(4);
    });
  }

  private _setScreenViewPort() {
    this.mobileAccessibility.setTextZoom(100);
    this.mobileAccessibility.usePreferredTextZoom(false);
  }
}
