import { Component } from '@angular/core';

@Component({
  selector: null,
  templateUrl: null
})

export class Language {

  support = {
    ko: {
      "Success payment": "결제 성공",
      "Update exfired Date, Thank you": "만료날짜가 업데이트 되었습니다.",
      "OK": "확인",
      "CANCEL": "취소",
      "ERROR": "에러",
      "Failed user data upload": "업로드에 실패하였습니다.",
      "Retry": "다시 시도",
      "Remove": "삭제",
      "Phone Number Verification": "번호 확인",
      "Required verification of your phone number": "번호 인증을 해주세요.",
      "CAUTION": "경고",
      "Wait few minute(max 2min) for Auto show": "자동보기를 위해 잠시 기다려주세요(최대 2분), 화면 보호기 시간을 늘려주세요!",
      "!!Auto Show required Login!!": "!!자동보기는 로그인이 필요합니다!!화면이 켜져있어야 동작합니다 :)",
      "Inquiry Status": "Inquiry Status",
      "Free Trial": "무료 체험",
      "Activated": "활성화",
      "Expired": "만료",
      "Paymentation": "결제",
      "month": "개월",
      "Get your free trial": "무료이용권을 얻으세요!",
      "3 Day !": "3일 이용권",
      "(Please click under button)": "(아래의 버튼을 눌러주세요)",
      "Show AD Video": "광고 보기",
      "autoShowTicket": "일 자동보기",
      "Auto show": "자동 보기",
      "Payment Required": "결제 요청",
      "Wait": "기다려주세요.",
      "Contry Codes": "국가 코드",
      "Identity Verification": "인증하기",
      "Contry code & Phone number": "국가코드 와 핸드폰 번호",
      "Code": "코드",
      "Please do not input dash(-)": "대쉬(-)는 빼고 입력해주세요.",
      "Send Verification MMS": "인증번호 전송",
      "correct phone number": "correct phone number",
      "please choice code number": "국가코드를 선택해주세요.",
      "incorrect phone number": "전화번호를 확인해 주세요.",
      "incorrect code": "인증번호를 확인해 주세요.",
      "Verification Code": "인증코드",
      "Please wait": "잠시만 기다려주세요",
      "Thank you, you get 3 free day": "감사합니다, 3일의 무료이용권을 획득하셨습니다.",
      "Can't get free day, you didn't saw video": "무료체험권을 얻지못했습니다. 동영상을 모두 보셔야 합니다.",
      "Please Wait for Auto Show" : "자동보기를 준비중입니다, 잠시만 기다려주세요",
      "Use to Another Device": "다른 기기에서 사용중입니다.",
      "Update this device?": "이기기로 재등록 하시겠습니까?",
      "Upgrade": "필수 업그레이드",
      "A mandatory upgrade exists. Please upgrade": "필수 업그레이드가 필요합니다. 업그레이드 해주세요"
    },
    en: {
      "autoShowTicket": "month auto show ticket"
    }
  };

  constructor() {

  }
}