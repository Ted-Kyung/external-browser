import { Component } from '@angular/core';

// post to server
import { HTTP } from '@ionic-native/http';
import { LoadingController } from 'ionic-angular';

/**
 how to make service class ???
 @Component <= required to this comment below
 import to app.module.ts
 add to app.module.ts @NgModule..provider []
 */
@Component({
  selector: null,
  templateUrl: null
})
export class FirebaseRestApiServices {

  //Rest
  REST_BASE_URL = "https://us-central1-futurebot-2ce20.cloudfunctions.net/";

  constructor(private http: HTTP,
              public loadingCtrl: LoadingController) {
  }

  restApiWithUrlAndData(url:string, values): Promise<any>{
    return new Promise((resolve, reject)=>{
      this.http.setHeader('*', 'Access-Control-Allow-Origin' , '*');
      this.http.setHeader('*', 'Access-Control-Allow-Methods', 'POST, GET, OPTIONS, PUT');
      this.http.setHeader('*', 'Access-Control-Allow-Headers', 'x-test-header, Origin, X-Requested-With, Content-Type, Accept');
      this.http.setHeader('*', 'Accept','application/json');
      this.http.setHeader('*', 'content-type','application/json');
      let headers = {
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Methods': 'POST, GET, OPTIONS, PUT',
        'Access-Control-Allow-Headers': 'x-test-header, Origin, X-Requested-With, Content-Type, Accept',
        'Accept': 'application/json',
        'content-type': 'application/json'
      };

      let request = url;
      this.http.setDataSerializer('json');
      console.log("rest api Data : " + JSON.stringify(values));
      this.http.post(request, values, headers) 
      .then(returnData => {
        console.log("respApi returned: " + JSON.stringify(returnData));
        resolve(JSON.parse(returnData['data']));
      }).catch(error=>{
        console.error(error);
        reject(error);
      });
    });
  }

  restGet(url:string): Promise<any>{
    return new Promise((resolve, reject)=>{
      let headers = {
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Methods': 'POST, GET, OPTIONS, PUT',
        'Access-Control-Allow-Headers': 'x-test-header, Origin, X-Requested-With, Content-Type, Accept',
        'Accept': 'application/json',
        'content-type': 'application/json'
      };
      this.http.get(url, {}, headers)
      .then(res=>{
        resolve(res);
      })
      .catch(err=>{
        reject("{}");
      });
    });
  }

  restApiWithData(requestType:string, values): Promise<any>{
    return new Promise((resolve, reject)=>{
      this.http.setHeader('*', 'Access-Control-Allow-Origin' , '*');
      this.http.setHeader('*', 'Access-Control-Allow-Methods', 'POST, GET, OPTIONS, PUT');
      this.http.setHeader('*', 'Access-Control-Allow-Headers', 'x-test-header, Origin, X-Requested-With, Content-Type, Accept');
      this.http.setHeader('*', 'Accept','application/json');
      this.http.setHeader('*', 'content-type','application/json');
      let headers = {
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Methods': 'POST, GET, OPTIONS, PUT',
        'Access-Control-Allow-Headers': 'x-test-header, Origin, X-Requested-With, Content-Type, Accept',
        'Accept': 'application/json',
        'content-type': 'application/json'
      };

      let request = this.REST_BASE_URL+"restAPI/"+requestType;
      this.http.setDataSerializer('json');
      console.log("rest api Data : " + JSON.stringify(values));
      this.http.post(request, values, headers) 
      .then(returnData => {
        console.log("respApi returned: " + JSON.stringify(returnData));
        resolve(JSON.parse(returnData['data']));
      }).catch(error=>{
        console.error(error);
        reject(error);
      });
    });
  }
}
