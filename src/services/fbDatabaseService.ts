import { Component } from '@angular/core';

import * as firebase from 'firebase/app';


/**
 how to make service class ???
 @Component <= required to this comment below
 import to app.module.ts
 add to app.module.ts @NgModule..provider []
 */
@Component({
  selector: null,
  templateUrl: null
})
export class FirebaseDatabaseServices {

  UserBasePath = '/userItems/';
  langData = {};

  constructor() {

  }

  public pushDataWithPath(path: string, data:any):Promise<any> {
    return new Promise((resolve, reject)=>{
      firebase.database().ref(`${path}`).push(data).then(Resp => {
        resolve(Resp);
      });
    });
  }

  public updateDataWithTokenStyle(basePath: string, token: string, data:any): Promise<any>{
    return new Promise(resolve => {
      let path = basePath + token;
      firebase.database().ref(`${path}`).update(data).then(resp => {
        resolve(resp);
      });
    });
  }

  public setDataWithTokenStyle(basePath: string, token: string, data:any): Promise<any>{
    return new Promise(resolve => {
      let path = basePath + token;
      firebase.database().ref(`${path}`).set(data).then(resp => {
        resolve(resp);
      });
    });
  }

  public registerFirebaseDb(path:string, callback=null) {
    var starCountRef = firebase.database().ref(`${path}`);
    starCountRef.on(`value`, (snapshot)=>{
      callback(snapshot.val());
    });
  }

  public getDataWithLength(path:string, startToken:string, len:number) : Promise<any>{
    return new Promise((resolve, reject)=>{
      if(!startToken) {
        firebase.database().ref(path).orderByKey()
        .limitToLast(len)
        .once('value')
        .then((snapshot)=>{
          resolve(snapshot.val())
        })
        .catch((error)=>{
          reject(error);
        });

      } else {
        firebase.database().ref(path).orderByKey()
        .endAt(startToken)
        .limitToLast(len)
        .once('value')
        .then((snapshot)=>{
          resolve(snapshot.val())
        })
        .catch((error)=>{
          reject(error);
        });
      }
    });
  }

  public getKeysFromItems(items:any) {
    let res:Array<any>;
    if(!!items){
      res = Object.keys(items);
    }
    else{
      res = [];
    }
    return res; 
  }

  public getItemWithPath(path:string): Promise<any> {
    return new Promise((resolve)=>{
      firebase.database().ref(`${path}`).once(`value`, (snapshot)=>{
        resolve(snapshot.val());
      });
    });
  }

  public getItemWithPathAndKey(rpath:string, tokKey:string, callback=null): Promise<any> {
    return new Promise((resolve)=>{
      let path = rpath+tokKey;
      firebase.database().ref(`${path}`).once(`value`, (snapshot)=>{
        if(callback){
          callback(snapshot.val());
        }
        resolve(snapshot.val());
      });
    });
  }
}
