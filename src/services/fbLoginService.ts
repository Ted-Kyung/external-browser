import { Component } from '@angular/core';

// using event api
import { LoadingController,AlertController } from 'ionic-angular';

// firebase login
// import { Facebook } from '@ionic-native/facebook';

import * as firebase from 'firebase';

/**
 how to make service class ???
 @Component <= required to this comment below
 import to app.module.ts
 add to app.module.ts @NgModule..provider []
 */
@Component({
  selector: null,
  template: ``,
  templateUrl: null,
})
export class FirebaseLoginService {

  UserBasePath = '/userItems/';
  storageLoginItems = 'loginItems';
  PATH_CODE = "";
  USERITEM= null;

  langData = {};

  confirmationResult: any;
  recaptchaVerifier:firebase.auth.RecaptchaVerifier;

  constructor(private alertCtrl:AlertController,
              private loadingCtrl: LoadingController,){
              // private facebook: Facebook) {
    this._setLanguage();
    firebase.auth().languageCode = 'en';
    this.recaptchaVerifier = new firebase.auth.RecaptchaVerifier('sign-in-button', {
      'size': 'invisible',
      'callback': function(response) {
        // reCAPTCHA solved, allow signInWithPhoneNumber.
      }
    });
  }

  _setLanguage(){
    this.langData = {
      OK: "OK",
      cancel: "Cancel",
      convertEmailToFacebook: "Need your Password for convert Facebook ID",
      loginErrMsgReCheck: "Please try again",
      loginErrMsgInvaildEmail: "Can't found E-mail",
      loginErrMsgInvalidPass: "Invalid Password",
      sendConfirmEmail: "Confirm E-mail",
      sendConfirmEmailDesc: "Please Check your E-mail",
      notYetConfirmEmailDesc: "Need to E-mail Verified",
      facebookHasNotMail: "Facebook Account has not e-mail address",
    }
  }

  // registration progress
  showRegesterFacebookUserAlert(){
    let loading = this.loadingCtrl.create({
        spinner: 'crescent',
        content: 'Create Account ..',
        duration: 2000
      });
    return loading;
  }

  // login progress 
  showLoginProgress(){
    let loading = this.loadingCtrl.create({
        spinner: 'crescent',
        content: 'Login ..',
        duration: 2000
      });
    return loading;
  }

  // show alert login failed reason 
  showLoginFailedAlert(error){
      var err_char = '';
      var err_desc = '';
      if((error['code'] == 'auth/user-not-found') || (error['code'] == 'auth/invalid-email')){
        err_char = this.langData['loginErrMsgInvaildEmail'];
        err_desc = this.langData['loginErrMsgReCheck'];
      } else if (error['code'] == 'auth/wrong-password') {
        err_char = this.langData['loginErrMsgInvalidPass'];
        err_desc = this.langData['loginErrMsgReCheck'];
      }
      else if (error['code'] == 'notEmail') {
        err_char = this.langData['loginErrMsgInvaildEmail'];
        err_desc = this.langData['facebookHasNotMail'];
      }
      console.log("error : ", error['code'])
      let alert = this.alertCtrl.create({
          title: err_char,
          subTitle: err_desc,
          buttons: ['OK']
        });
      alert.present();
  }

  // update password, using into convert email to facebook
  updatePassword(pass):Promise<any>{
    return new Promise((resolve, reject)=>{
      var user = firebase.auth().currentUser;
      user.updatePassword(pass).then((res)=>{
        resolve(res);
      }).catch((error)=>{
        reject(error);
      });
    });
  }

  // if first registration from facebook...
  generateFromFaceBookInfo(lgEmail, lgPass, displayName):Promise<any>{
    return new Promise((resolve, reject)=>{
      var progress = this.showRegesterFacebookUserAlert();
      progress.present();
      this.createAccountWithEmail(lgEmail, lgPass, displayName)
        .then((cRes)=>{
          resolve(cRes);
        })
        .catch((cError)=>{
          progress.dismiss();
          reject(cError);
      });
    });
  }

  // send to verification email
  sendEmailVerification(user):Promise<any>{
    return new Promise((resolve, reject)=>{
      firebase.auth().languageCode = 'en';
      user.sendEmailVerification().then((res)=>{
        console.log("res : " + JSON.stringify(res));
        resolve(res);
      }).catch(function(error) {
        console.error("error : " + error);
        reject(error);
      });
    });
  }
  // if user account email not yet verification   
  showEmailIsNotVerified(user){
    var sendCheckEmailConfirmAlert = this.alertCtrl.create({
      title: this.langData['sendConfirmEmail'],
      subTitle:this.langData['notYetConfirmEmailDesc'],
      buttons: [{
          text: this.langData['cancel'],
          handler: () => {}
        },
        {
          text: this.langData['OK'],
          handler: () => {
            console.log("showEmailIsNotVerified : " + ", " + JSON.stringify(user));
            this.sendEmailVerification(user).then((res)=>{}).catch((err)=>{});
          }
        }]
    });
    sendCheckEmailConfirmAlert.present();
  }

  // if registration account, send to Verification email
  showPleaseConfirmUserEmailAlert(user):Promise<any>{
    return new Promise((resolve, reject)=>{
      console.log("!!!!!!!!! start showConfirmUserEmail");
      var sendCheckEmailConfirmAlert = this.alertCtrl.create({
        title: this.langData['sendConfirmEmail'],
        subTitle:this.langData['sendConfirmEmailDesc'],
        buttons: [{
            text: this.langData['OK'],
            handler: () => {}
          }]
      });
      sendCheckEmailConfirmAlert.onDidDismiss(()=>{
        firebase.auth().languageCode = 'en';
        console.log("showConfirmUserEmail : " + ", " + JSON.stringify(user));
        this.sendEmailVerification(user).then((res)=>{resolve(res);}).catch((error)=>{reject(error);});
      });
      sendCheckEmailConfirmAlert.present();
    });
  }

  // convert Email to facebook flow
  // try login original email with password
  // change password to facebook user acccount id after email login
  // retry email login with password(facebook id)
  convertEmailToFaceBookPassword(lgEmail, lgPass):Promise<any>{
    return new Promise((resolve, reject)=>{
      var convertAlert = this.alertCtrl.create({
        title: this.langData['convertEmailToFacebook'],
        inputs: [{
            name: 'password',
            placeholder: 'Password',
            type: 'password'
          }],
        buttons: [{
            text: this.langData['cancel'],
            role: 'cancel',
            handler: data => {
              console.log('Cancel clicked');
            }
          },
          {
            text: this.langData['OK'],
            handler: data => {
              this.loginWithEmail(lgEmail, data.password).then((res)=>{
                this.updatePassword(lgPass);
                resolve(res);
              }).catch((error)=>{
                console.error(error);
                reject(error);
              });
            }
          }]
      });
      convertAlert.present();
    });
  }

  private isAlreadyRegistrationEamil(error){
    return (error['code'] == 'auth/wrong-password');
  }

  private isFirstTimeLoginFromFacebook(error){
    return (error['code'] == "auth/user-not-found");
  }

  // try reset account password
  public resetPasswardEmailSend(email:string):Promise<any>{
    return new Promise((res, rej)=>{
      firebase.auth().sendPasswordResetEmail(email).then(r=>{res(r);}).catch(e=>{rej(e);})
    });
  }

  // login with email address, but try from facebook, don't show error alert 
  // and management login progress modal
  public loginWithEmail(email: string, pass: string, from=null): Promise<any> {
    return new Promise((resolve, reject)=>{
      var progress = this.showLoginProgress();
      progress.present();
      firebase.auth().signInWithEmailAndPassword(email, pass).then(response => {
        let user = firebase.auth().currentUser;
        resolve(user);
      }).catch(error => {
        reject(error);
      });
    });
  }

  // Check email athented , if login to test account pass this checker
  public checkAuthentedEmailUser(user):Promise<any>{
    return new Promise((resolve, reject)=>{
      var domain = String(user.email).split('@')[1];
      if(domain == 'test.com'){
        resolve();
      }
      if(user.emailVerified){
        resolve();
      }else{
        reject();
      }
    });
  }

  // return useritem of last login user 
  public getFbCurrentUserItem(){
    console.log("email : " + firebase.auth().currentUser.email);
    return firebase.auth().currentUser;
  }

  public removeUserAccount(email: string, pass: string):Promise<any>{
    return new Promise((resolve, reject)=>{
      this.loginWithEmail(email, pass)
      .then(res=>{
        let user = firebase.auth().currentUser;
        user.delete().then((res)=>{
          resolve();
        })
        .catch((err)=>{
          reject(err);
        });
      })
      .catch(err=>{
        reject(err);
      });
    });
  }

  // create email account to firebase database
  public createAccountWithEmail(email: string, pass: string, displayName:string):Promise<any>{
    return new Promise((resolve, reject) => {
      console.log("email: " + email);
      firebase.auth().createUserWithEmailAndPassword(email, pass).then(response => {
        console.log("got response : ", response);
        var user = firebase.auth().currentUser;
        resolve(user);
      }).catch(error => {
        console.log("got error : ", error);
        reject(error);
      });
    });
  }
 
  public getCurrentUserId(){
    return firebase.auth().currentUser.uid;
  }

  public phoneNumberVerifiedCodeConfirm(code):Promise<any>{
    return new Promise((resolve, reject)=>{
      this.confirmationResult.confirm(code)
      .then(function (result) {
        console.log(result.user);
        resolve(result.user);
      }).catch(function (error) {
        console.log("invalid code : " + error);
        reject(null);
      });
    });
  }

  // phone number verification 
  public async phoneNumberVerification(number:string){
    try{
      const appVerifier = this.recaptchaVerifier;
      const phoneNumberString = number;
      
      this.confirmationResult = await firebase.auth().signInWithPhoneNumber(phoneNumberString, appVerifier);
      appVerifier.verify();
      return this.confirmationResult;
    } catch {
      throw new Error('Failed to send sms');
    }
  }
}
