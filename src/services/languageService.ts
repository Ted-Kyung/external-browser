import { Component } from '@angular/core';

// using event api
import { Platform } from 'ionic-angular';

// // Import language data
import { Language } from './i18n/language';

@Component({
  selector: null,
  templateUrl: null
})

export class LangServices {

  langData = null;

  constructor(private platform: Platform,
              private langauage: Language) {
    this.platform.ready();
  }

  getLangData(key) {
    this._setLang();

    let langData: any = this.langauage.support[this.langData];

    if (!!langData) {
      if (!!langData[key]) {
        return langData[key];
      }
    }

    return key;
  }

  private _setLang() {
    if (!!this.langData) {
      // Already set
    } else {
      this.langData = window.navigator.language.substr(0, 2).toLowerCase();
    }
  }
}