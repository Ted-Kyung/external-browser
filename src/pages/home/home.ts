
import { Component, enableProdMode, ViewChild} from '@angular/core';
import { NavController, Platform, AlertController, LoadingController, Events} from 'ionic-angular';
import { HttpClient } from '@angular/common/http';

// external
import { AdMobFree, AdMobFreeBannerConfig, AdMobFreeRewardVideoConfig} from '@ionic-native/admob-free';

// local storage
import { Storage } from '@ionic/storage';

// rest API
import { FirebaseRestApiServices } from '../../services/fbRestApi';
import { LangServices } from '../../services/languageService';

// paypal
// import { PayPal, PayPalPayment, PayPalConfiguration } from '@ionic-native/paypal';
import { InAppPurchase } from '@ionic-native/in-app-purchase';

import { AndroidPermissions } from '@ionic-native/android-permissions/ngx';

// plugin
import { AppVersion } from '@ionic-native/app-version';

enableProdMode()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  // constans
  USER_UID = "userid";

  // user
  userItem
  localUserItem

  // ui
  userPhoneNumberText = "Unknown";
  paymentType:number = -1;
  userStatus:string = "Inquiry Status";
  exfiredDate:string = "";
  // open directive handler
  openLoginPage = false ;
  buttonType = 0;
  buttonText = "Wait";
  webBrowser:any;
  webBrowser2:any;

  automationCount = 2;

  productsInfo: any = null;
  productsItem: any = null;

  price1 = "$5.99"
  price6 = "$30.99"
  price12 = "$59.99"

  //version
  _publicVersion = "";
  _currentVersion = "";

  serverStatus = "Bad Server";
 
  rewardedStatus = false;

  banner_code = "ca-app-pub-2532707263134374/3621512631";
  // banner_code = "ca-app-pub-3940256099942544/6300978111";   // test 
  reward_code = "ca-app-pub-2532707263134374/4102279462";
  // reward_code = "ca-app-pub-3940256099942544/5224354917";   // test

  constructor(private http: HttpClient,
              private events: Events,
              public navCtrl: NavController,
              public andPermission: AndroidPermissions,
              public admob: AdMobFree,
              public storage: Storage,
              public version: AppVersion,
              private inApp: InAppPurchase,
              public loading:LoadingController,
              public restApi: FirebaseRestApiServices,
              public platform: Platform,
              public alertCtrl:AlertController,
              private langSrv: LangServices) {
    this.platform.ready().then(()=>{
      this._active();
      this.prepareBanner();
      this.prepareReward();

      this._getProducts();
      this.registEventSubscribe();

      // Throw error (Please check: TED)
      // if(this.platform.is('android')){
      //   this.andPermission.checkPermission(this.andPermission.PERMISSION.CAMERA).then(
      //     result => console.log('Has permission?',result.hasPermission),
      //     err => this.andPermission.requestPermission(this.andPermission.PERMISSION.CAMERA)
      //   );
      // }

    });
    this.events.unsubscribe("display");
    this.events.subscribe("display", ()=>{this.display();});
  }
  private _checkVersionNumbers(){
    let _currentVersionArray = this._currentVersion.split('.');
    let _publicVersionArray= this._publicVersion.split('.');
    console.log("cV : " + this._currentVersion + ", cvA : " + _currentVersionArray + ", pV : " + this._publicVersion + ", pvA : " + _publicVersionArray);
    if((_publicVersionArray[0] > _currentVersionArray[0])||
      (_publicVersionArray[1] > _currentVersionArray[1])||
      (_publicVersionArray[2] > _currentVersionArray[2])){
      console.log("do must app upgrade");

      let cTopic = this.langSrv.getLangData("Upgrade");
      let cTextBody = this.langSrv.getLangData("A mandatory upgrade exists. Please upgrade");

      let _upgrade = this.alertCtrl.create({
        title: cTopic,
        subTitle: cTextBody,
        buttons: [{text: "OK"}]
      });
      _upgrade.onDidDismiss(()=>{
        window.open('http://onelink.to/ew86ek', "_system");
      });
      _upgrade.present();
    }
    else {
      console.log("public version");
    }
  }

  private _versionChecker(){
    this.version.getVersionNumber().then(_cV=>{
      this._currentVersion = _cV;
      if(this._publicVersion.length === 0){
        this.restApi.restApiWithData("getVersion", {}).then(_pV => {
          console.log("public source server .. ");
          this._publicVersion = _pV.result;
          this._checkVersionNumbers();
        });
      }
      else{
        console.log("public source local .. ");
        this._checkVersionNumbers();
      }
    }).catch(error=>{
      console.log("error : " + error );
    });
 }

  public makeSecretKey() {
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
  
    for (var i = 0; i < 5; i++)
      text += possible.charAt(Math.floor(Math.random() * possible.length));
  
    return text;
  }

  public openInfo() {
    window.open('https://open.kakao.com/o/socTEMhb', "_system")
  }

  ionViewDidLoad(){
    this.display();
  }
  
  registEventSubscribe(){
    let slackUrl = 'https://hooks.slack.com/services/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';
    console.log("registEventSubscribe");
    document.removeEventListener('admob.rewardvideo.events.CLOSE', (event) => {
      console.log("NNNN remove listner admob CLOSE");
    });
    document.removeEventListener('admob.rewardvideo.events.OPEN', (event) => {
      console.log("NNNN remove listner  OPEN");
    });
    document.removeEventListener('admob.rewardvideo.events.REWARD', (event) => {
      console.log("NNNN remove listner  REWARD");
    });
    
    document.addEventListener('admob.rewardvideo.events.CLOSE', (event) => {
      console.log("NNNN admob CLOSE");
      if(this.rewardedStatus == false){
        alert(this.langSrv.getLangData("Can't get free day, you didn't saw video"));
      }
      this.prepareReward();
    });
    document.addEventListener('admob.rewardvideo.events.OPEN', (event) => {
      console.log("NNNN admob OPEN");
      this.rewardedStatus = false;
    });
    document.addEventListener('admob.rewardvideo.events.REWARD', async (event) => {
      this.rewardedStatus = true;
      console.log("NNNN admob REWARD");
      let _res = await this.restApi.restApiWithData("paymentSuccess", {user:this.userItem, paymentType: 1000});
      this.updateUserItem(_res.result)
      let msg = {"text": "ADMOB Reward Video Success, user id : " + this.userItem.uid + ", number : " + this.userItem.phoneNumber};
      await this.restApi.restApiWithUrlAndData(slackUrl, msg);
      alert(this.langSrv.getLangData("Thank you, you get 3 free day"));
      this.prepareReward();
    });
  }

  async display() {
    if(!!this.localUserItem){
      let res = await this.restApi.restApiWithData("getUserItem", {phoneNumber: this.localUserItem.phoneNumber});
      console.log("user item from server : " + JSON.stringify(res));
      if(!!res.result){
        console.log("Success load from Server");
        this.updateUserItem(res.result);
      } else {
        console.log("failed load from Server");
        this.removeUserItem();
      }

      let secretRes = await this.restApi.restApiWithData("checkSecretKey", this.localUserItem);
      console.log("check secret");
      if(!!!secretRes.result){
        console.log("Success load from Server");
        let _alert = this.alertCtrl.create({
          title: this.langSrv.getLangData("Use to Another Device"),
          subTitle: this.langSrv.getLangData("Update this device?"),
          buttons:[{
            text: this.langSrv.getLangData("OK"),
            handler: ()=>{this.openLoginPage = true;}
          },{
            text: this.langSrv.getLangData("CANCEL"),
            handler: ()=>{}
          }],
        });
        _alert.onDidDismiss(async ()=>{
          this.removeUserItem();
        });
        _alert.present();
      }
    }
  }

  private async _getProducts() {
    try {
      console.log('Get products');
      this.productsInfo = await this._getProductsInfo();
      console.log("Products info from jsonfile", this.productsInfo);

      this.productsItem = await this._getProductItems();
      console.log("Products item from playstore", this.productsItem);
    } catch(error) {
      console.log('Get products error.', error);
    }
  }

  private _getProductItems() {
    return new Promise(async (resolve, reject) => {
      try {
        let products = await this.inApp.getProducts([this.productsInfo.future_1000.id,
                                                     this.productsInfo.future_2000.id,
                                                     this.productsInfo.future_3500.id]);

        console.log('NNNNN Get product item : ', JSON.stringify(products));
        products.forEach(p => {
          if(p.productId === "future_1"){
            this.price1 = p.price;
          } else if(p.productId === "future_6"){
            this.price6 = p.price;
          } else if(p.productId === "future_12"){
            this.price12 = p.price;
          } else if(p.productId === "future_1000"){
            this.price1 = p.price;
          } else if(p.productId === "future_2000"){
            this.price6 = p.price;
          } else if(p.productId === "future_3500"){
            this.price12 = p.price;
          }
        });
        resolve(products);
      } catch(error) {
        console.log('Get product item error', error);
        reject(error);
      }

    });
  }

  private _getProductsInfo() {
    return new Promise((resolve, reject) => {
      this.http.get('assets/data/inapp_products.json').subscribe((data) => {
        resolve(data);
      }, (error) => {
        reject(error);
      });
    })
  }

  paymentTlie(type:number){
    if(this.paymentType === type){
      this.paymentType = -1;
      return;
    }
    this.paymentType = type;
    var rand = Math.random()
    if(rand > 0.4){
      this.admob.banner.hide();
    } else {
      this.admob.banner.show();
    }
  }

  paymentPrice(index) {
    if (!!this.productsItem) {
      return '$' + this.productsItem[index].price;
    }
  }

  private async _buyProduct(productId) {
    let slackUrl = 'https://hooks.slack.com/services/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';
    try{
      let _restore:Array<any> = await this.inApp.restorePurchases();

      var len = _restore.length;
      for(var i = 0; i < len; i++){
        await this.inApp.consume(_restore[i].productType, _restore[i].receipt, _restore[i].signature);
      }

      await this.inApp.buy(this.productsInfo[productId].id);
      let _res = await this.restApi.restApiWithData("paymentSuccess", {user:this.userItem, paymentType: this.paymentType});
      this.updateUserItem(_res.result);
      let msg = {"text": "!!!!!! Payment Success type : " + this.paymentType + ", user id : " + this.userItem.uid + ", number : " + this.userItem.phoneNumber};
      await this.restApi.restApiWithUrlAndData(slackUrl, msg);
      alert(this.langSrv.getLangData('Succeed buy item'));
    } catch (e) {
      //validation check
      if(!(e.hasOwnProperty("code")) !! !(e.hasOwnProperty("errorCode"))) return;
      if(e.code === -5) return;
      let msg = {"text": "Payment Failed, Reason : " + JSON.stringify(e) + ", user id : " + this.userItem.uid + ", number : " + this.userItem.phoneNumber};
      this.restApi.restApiWithUrlAndData(slackUrl, msg);
      alert(this.langSrv.getLangData('Failed buy item'));
    }
  }

  // private _buySubscribe(productId) {
  //   return new Promise(async(resolve, reject) => {
  //     // await this.inApp.subscribe(this.productsInfo[productId].id);

  //     alert(this.productsInfo[productId].range + ' 개월만큼 사용할 수 습니다.');
  //   });
  // }

  getButtonStyle(){
    var style = {};
    if(this.buttonType == 0){
      style['color'] = '#f0f0f0';
      style['background-color'] = "rgba(140, 136, 137, 1)";
    }
    else if(this.buttonType == 1){
      style['color'] = '#f0f0f0';
      style['background-color'] = "#9ab4dc";
    }
    return style;
  }

  getStatusColor(){
    this.buttonText = this.langSrv.getLangData("Wait");

    if((this.userStatus === "Free Trial") || (this.userStatus === "Activated")){
      this.buttonType = 1;
      this.buttonText = `${this.automationCount} ` + this.langSrv.getLangData('Auto show');
      return {'color':'rgb(64, 141, 235)', 'font-size': '2.8em'};
    }
    else {
      this.buttonType = 0;
      this.buttonText = this.langSrv.getLangData("Payment Required");
      return {'color':'#ca1514', 'font-size': '2.0em'};
    }


  }

  getExpiredColor(){
    if((this.userStatus === "Free Trial") || (this.userStatus === "Activated")){
      return {'color':'rgb(64, 141, 235)'};
    }
    else {
      return {'color':'#ca1514'};
    }
  }

  autoShowCount(value:number){
    this.automationCount = this.automationCount + value;
    if(this.automationCount <= 2) this.automationCount = 2;
    if(this.automationCount >= 20) this.automationCount = 20;
    var rand = Math.random()
    if(rand > 0.4){
      this.admob.banner.hide();
    } else {
      this.admob.banner.show();
    }
  }

  getPaymentTileStyle(type:number){
    if(this.paymentType === type){
      return {'background-color':'#b8ffaf', 'color': '#508251'};
    }
    else {
      return {'background-color':'#b5b1b2', 'color': 'white'};
    }
  }

  prepareBanner() {
    let bannerConfig: AdMobFreeBannerConfig = {
        isTesting: false,
        autoShow: false,
        id: this.banner_code
    };
    this.admob.banner.config(bannerConfig);
    this.admob.banner.prepare().then(() => {
      this.admob.banner.show();
    }).catch(e => console.log(e));
  }
  prepareReward() {
    let rewardConfig: AdMobFreeRewardVideoConfig = {
        isTesting: false,
        autoShow: false,
        id: this.reward_code
    };
    this.admob.rewardVideo.config(rewardConfig);
    this.admob.rewardVideo.prepare().then(() => {
    }).catch(e => console.log(e));
  }

  async paymentClick(){
    console.log("payment button click!! type : " + this.paymentType);
    var payDescription = "";
    var payRequest:string = '0.00';
    if(this.paymentType === 1000){
      payDescription = '1' + this.langSrv.getLangData('autoShowTicket');
    } else if (this.paymentType === 2000){
      payDescription = '7' + this.langSrv.getLangData('autoShowTicket');
    } else if (this.paymentType === 3500){
      payDescription = '15' + this.langSrv.getLangData('autoShowTicket');
    } else {
      console.log("invalid paymentType");
      return ;
    }
    const _l = await this.loading.create({ spinner: 'crescent', duration: 5000 });
    _l.present();
    // await this._buyProduct("future_" + this.paymentType);
    // _l.dismiss();
    this.admob.rewardVideo.isReady().then(res=>{
      console.log("NNNN reward isready success -> show");
      this.admob.rewardVideo.show();
    }).catch(e=>{
      console.log("NNNN reward show err: " + e);
    })
  }

  removeUserItem(){
    this.userItem = null;
    this.userStatus = 'Invalid user';
    this.exfiredDate = 'xxxx-xx-xx';
    this.userPhoneNumberText = '???????';
    this.exfiredDate = this.exfiredDate.replace(/\_/gi, "-")
  }

  updateUserItem(data){
    this.userItem = data;
    this.userStatus = data.status;
    this.exfiredDate = data.expiredDate;
    this.userPhoneNumberText = data.phoneNumber;
    this.exfiredDate = this.exfiredDate.replace(/\_/gi, "-")
  }


  public async handleLoginPage(data){
    console.log("handle login page : " + JSON.stringify(data));
    if(data.isNew){
      const _l = await this.loading.create({ spinner: 'crescent', duration: 5000 });
      _l.present();
      let res = await this.restApi.restApiWithData("createNewUser", data.user);
      console.log("handleLogin : " + JSON.stringify(res));
      _l.dismiss();
    }

    if(!!data.user){
      data.user['secretKey'] = this.makeSecretKey();
      await this.restApi.restApiWithData("setSecretKey", data.user);
      this.storage.set(this.USER_UID, data.user);
      this.updateUserItem(data.user);
      this.openLoginPage = false;
    } else {
      let _alert = this.alertCtrl.create({
        title: this.langSrv.getLangData("ERROR"),
        subTitle: this.langSrv.getLangData("Failed user data upload"),
        buttons:[{
          text: this.langSrv.getLangData('Retry'),
          handler: ()=>{
            this.handleLoginPage(data);
          }
        }],
      });
      _alert.present();
    }
  }

  private async _active() {
    // check server
    this.restApi.restGet("https://adpro.futurenet.club/")
    .then((res)=>{
      console.log("NNNNNN res : " + JSON.stringify(res));
      if(res.status === 200){
        (document.getElementById("server") as HTMLImageElement).src = "assets/svg/good_server.svg";
        this.serverStatus = "Good Server";
      } else {
        (document.getElementById("server") as HTMLImageElement).src = "assets/svg/bad_server.svg";
        this.serverStatus = "Bad Server";
      }
    })
    .catch((error)=>{
      console.log("NNNNNN error : " + JSON.stringify(error));
      (document.getElementById("server") as HTMLImageElement).src = "assets/svg/bad_server.svg";
      this.serverStatus = "Bad Server";
    });
    this._versionChecker();
    // loding ctrl
    const _l = await this.loading.create({ spinner: 'crescent', duration: 5000 });
    _l.present();
    // 1. check storage uid
    this.localUserItem = await this.storage.get(this.USER_UID);
    console.log("storage User : " + JSON.stringify(this.localUserItem));
    if(!!this.localUserItem){
      // already has uid, go to main
      console.log("found user id : " + this.localUserItem.phoneNumber);
      this.userPhoneNumberText = this.localUserItem.phoneNumber;
      this.openLoginPage = false;
      this.display();
      _l.dismiss();
      if(this.userItem === null){
        let _alert = this.alertCtrl.create({
          title: this.langSrv.getLangData("ERROR"),
          subTitle: this.langSrv.getLangData("Failed user data upload") + "(" + this.localUserItem.phoneNumber + ")",
          buttons:[{
            text: this.langSrv.getLangData("Remove"),
          }],
        });
        _alert.onDidDismiss(()=>{
          this.storage.remove(this.USER_UID);
          this._active();
        });
        _alert.present();
      }
    } else {
      // has not uid, go to login by phone number
      _l.dismiss();
      console.log("can not found user id");
      let _alert = this.alertCtrl.create({
        title: this.langSrv.getLangData("Phone Number Verification"),
        subTitle: this.langSrv.getLangData("Required verification of your phone number"),
        buttons:[{
          text: this.langSrv.getLangData("OK"),
        }],
      });
      _alert.onDidDismiss(()=>{
        this.openLoginPage = true;
      });
      _alert.present();
    }
  }

  /////// automation flow

  startAlertAutoShow(){
    if(this.buttonType==0) return;
    let _alert = this.alertCtrl.create({
      title: this.langSrv.getLangData("CAUTION"),
      subTitle: this.langSrv.getLangData("Wait few minute(max 2min) for Auto show"),
      buttons:[{
        text: this.langSrv.getLangData("OK"),
      }],
    });
    let _alert2 = this.alertCtrl.create({
      title: this.langSrv.getLangData("CAUTION"),
      subTitle: this.langSrv.getLangData("!!Auto Show required Login!!"),
      buttons:[{
        text: this.langSrv.getLangData("OK"),
      }],
    });
    _alert.onDidDismiss(()=>{_alert2.present()});
    _alert2.onDidDismiss(()=>{this.startAutoAdvertising()});
    _alert.present();
  }

  async startAutoAdvertising(){
    const LOGIN_CHECK = 0;
    const MAINPAGE_CHECK = 1;
    const ALREADY_OPENADS_CHECK = 2;
    const OPENADS_CHECK = 3;
    const CLOSE_CHECK = 4;

    let automationRequestCount = this.automationCount;
    if(this.buttonType==0) return;

    const _l = await this.loading.create({ spinner: 'crescent', content: this.langSrv.getLangData("Please Wait for Auto Show"),duration: 180000 });
    _l.present();

    this.webBrowser2 = window.open("http://adpro.futurenet.club/", "_blank", "hide=yes" );
    this.webBrowser2.addEventListener("loadstop", ()=>{
      _l.dismiss();
      this.webBrowser = window.open("http://adpro.futurenet.club/advertising/watch-ads", "_blank", "location=yes,hidespinner=no" );
      this.webBrowser.addEventListener( "loadstop", () => {
        console.log('Loaded');
        this.webBrowser.executeScript({
          file: "https://code.jquery.com/jquery-1.10.2.js"
        },()=>{
          console.log("Autoshow flow start");
          setTimeout(() => {
            this.webBrowser.executeScript({
              code: 
              `
              var closeButtonCheckCount = 0;
              var currentCounting = 0;
              var automationRequestCount = ${automationRequestCount};
              var onceLoginAlert = false;
              var flowStatus = ${LOGIN_CHECK};
              var checkPoint = null;
              intervalID = setInterval(()=>{
                console.log("interval id : " + intervalID);
                if(flowStatus === ${LOGIN_CHECK}){
                  console.log("login check");
                  checkPoint = null;
                  checkPoint = $('button.btn.btn-link')[0];
                  if(!!checkPoint){
                    console.log("current page : login");
                    if((checkPoint.innerText === 'LOG IN') && (!onceLoginAlert)) {
                      console.log("show alert");
                      onceLoginAlert = true;
                      alert('[Future AutoBot] Please login and restart AutoShow');
                    }
                    // else -> do not anything
                  } else {
                    console.log("not login page go to main check");
                    flowStatus = ${MAINPAGE_CHECK};
                  }
                }

                // main page check
                if(flowStatus === ${MAINPAGE_CHECK}){
                  console.log("main check");
                  checkPoint = null;
                  checkPoint = $('button.no-background.ad__button.btn.btn-primary')[0];
                  if(!!checkPoint){
                    console.log("find 'ADS OPEN' button");
                    if(checkPoint.innerText !== 'OPEN'){
                      console.log("current page : main click 'wadch ads' button");
                      $('a.navbar-watch-ads__btn.btn-primary')[0].click();
                    } else {
                      console.log("got some error, go to ALREADY_OPENADS_CHECK");
                      flowStatus = ${ALREADY_OPENADS_CHECK};
                    }
                  } else {
                    console.log("not main page go to Already open ads check ");
                    flowStatus = ${ALREADY_OPENADS_CHECK};
                  }
                }

                // open ads modal check
                if(flowStatus === ${ALREADY_OPENADS_CHECK}){
                  console.log("already open ads check");
                  checkPoint = null;
                  checkPoint = $('.modal.fade.show')[0];
                  if(!!checkPoint){
                    console.log("already open ads, go to 'close button'");
                    $("#ad-watch-modal-iframe").trigger("mouseover");
                    flowStatus = ${CLOSE_CHECK};
                  } else {
                    console.log("not yet open ads, go to 'open ads button'");
                    flowStatus = ${OPENADS_CHECK};
                  }
                }

                // open ads
                if(flowStatus === ${OPENADS_CHECK}){
                  console.log("find ads button");
                  checkPoint = null;
                  checkPoint = $('button.no-background.ad__button.btn.btn-primary')[0];
                  if(!!checkPoint){
                    console.log("discover ads button, click");
                    closeButtonCheckCount = 0;
                    checkPoint.click();
                    flowStatus = ${ALREADY_OPENADS_CHECK};
                  } else {
                    console.log("can not found ads button, go to Login");
                    flowStatus = ${LOGIN_CHECK};
                  }
                }

                // close button
                if(flowStatus === ${CLOSE_CHECK}){
                  closeButtonCheckCount++;
                  console.log("find close button");
                  checkPoint = null;
                  checkPoint = $("#ad-watch-modal-iframe button.btn.btn-primary")[0];
                  if(!!checkPoint){
                    console.log("discover some button");
                    if(checkPoint.innerText === 'NEXT AD'){
                      console.log("discover close button");
                      checkPoint.click();
                      currentCounting++;
                      if(currentCounting >= automationRequestCount){
                        // success auto show done
                        clearInterval(intervalID);
                        alert('[Future AutoBot] ${automationRequestCount} ADS show completed');
                      }
                    }
                    if(closeButtonCheckCount >= 80){
                      flowStatus = ${OPENADS_CHECK};
                    }
                  } else {
                    // can not found ads button go to 0
                    console.log("can not found close button");
                    flowStatus = ${ALREADY_OPENADS_CHECK};
                  }
                }
              }, 1500);`
            });
          }, 3000);
        });
      });
    });
  }
}